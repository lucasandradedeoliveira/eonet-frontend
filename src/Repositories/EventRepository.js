import axios from 'axios';

const baseUrl = process.env.REACT_APP_API_URL || 'https://localhost:5001/events';

class EventRepository {
    async list(status, days, order) {
        let url = `${baseUrl}?status=${status}&`;

        if (days !== null) {
            url += `days=${days}&`;
        }

        if (order) {
            url += `order=${order}&`;
        }

        try {
            const response = await axios.get(url);
            return response.data;
        } catch (e) {
            console.error(e);
        }

        return [];
    }

    async get(eventId) {
        try {
            const response = await axios.get(`${baseUrl}/${eventId}`);
            return response.data;
        } catch (e) {
            console.error(e);
        }

        return [];
    }
}

const instance = new EventRepository();

export default instance;
