import React from 'react';
import { Modal } from 'react-bootstrap';

function EventModal(props) {
    const { event } = props;

    const hasEvent = event && event.title;

    const renderBody = () => {
        if (!hasEvent) {
            return null;
        }

        return (
            <Modal.Body>
                <p>
                    <strong>Categories</strong>
                    <br />
                    {event.categories.map(c => (
                        <span className="badge badge-primary" key={c.id}>{c.title}</span>)
                    )}
                </p>
                <p>
                    <strong>Sources</strong>
                    <br />
                    {event.sources.map((s, i) => (
                        <span className="badge badge-primary" key={i}>{s.id}: {s.url}</span>)
                    )}
                </p>
            </Modal.Body>
        );
    };

    return (
        <Modal show={event !== null} onHide={props.onClose} keyboard={false}>
            <Modal.Header closeButton={hasEvent}>
                <Modal.Title>{hasEvent ? event.title : 'Loading'}</Modal.Title>
            </Modal.Header>
            {renderBody()}
        </Modal>
    );
}

export default EventModal;
