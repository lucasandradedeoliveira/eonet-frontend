import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import { Button, ButtonGroup } from 'react-bootstrap';

import 'react-datepicker/dist/react-datepicker.css';

function Form(props) {
    const [startDate, setStartDate] = useState(null);

    const onChangeDate = date => {
        if (props.loading) {
            return;
        }

        const now = new Date();
        const diff = now.getTime() - date.getTime();
        const days = Math.floor(diff / (1000 * 3600 * 24));

        setStartDate(date);
        props.onChangeDays(days);
    };

    const orderStatusVariant = props.order === 'status' ? 'primary' : 'secondary';
    const orderCategoryVariant = props.order === 'category' ? 'primary' : 'secondary';
    const statusOpenVariant = props.status === 'open' ? 'primary' : 'secondary';
    const statusClosedVariant = props.status === 'closed' ? 'primary' : 'secondary';

    return (
        <div className="row">
            <div className="col-auto">
                <div className="form-group">
                    <label className="d-block">Date</label>
                    <DatePicker
                        className="form-control"
                        selected={startDate}
                        onChange={onChangeDate}
                        maxDate={new Date()} />
                </div>
            </div>
            <div className="col-auto">
                <div className="form-group">
                    <label className="d-block">Status</label>
                    <ButtonGroup>
                        <Button variant={statusOpenVariant} onClick={props.onToggleStatus}>Open</Button>
                        <Button variant={statusClosedVariant} onClick={props.onToggleStatus}>Closed</Button>
                    </ButtonGroup>
                </div>
            </div>
            <div className="col-auto">
                <div className="form-group">
                    <label className="d-block">Order by</label>
                    <ButtonGroup>
                        <Button
                            variant={orderCategoryVariant}
                            onClick={() => props.onChangeOrder('category')}>
                            Category
                        </Button>
                        <Button
                            variant={orderStatusVariant}
                            onClick={() => props.onChangeOrder('status')}>
                            Status
                        </Button>
                    </ButtonGroup>
                </div>
            </div>
        </div>
    );
}

export default Form;
