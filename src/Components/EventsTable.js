import React from 'react';
import EventRow from './EventRow';

function EventsTable(props) {
    const renderEmpty = () => {
        return (
            <tr key="empty">
                <td colSpan="4" className="text-center">
                    <h3>No events found</h3>
                </td>
            </tr>
        );
    };

    const renderLoading = () => {
        return (
            <tr key="loading">
                <td colSpan="4" className="text-center">
                    <h3>Loading</h3>
                </td>
            </tr>
        );
    };

    const rows = [];

    if (props.loading) {
        rows.push(renderLoading());
    } else if (props.events.length < 1) {
        rows.push(renderEmpty());
    }

    if (props.events.length > 0) {
        rows.push(props.events.map(e => (
            <EventRow key={e.id} {...e} onClick={props.onClickEvent} />
        )));
    }

    return (
        <table className="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Categories</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </table>
    );
}

export default EventsTable;
