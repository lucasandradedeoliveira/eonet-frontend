import React from 'react';

function EventRow(props) {
    const classeList = [];

    if (props.closed) {
        classeList.push('table-secondary');
    }

    const renderCategories = () => {
        return props.categories.map(c => (
            <span className="badge badge-primary" key={c.id}>{c.title}</span>)
        );
    };

    return (
        <tr className={classeList.join(' ')} onClick={() => props.onClick(props.id)}>
            <td>{props.id}</td>
            <td>{props.title}</td>
            <td>{renderCategories()}</td>
            <td>{props.closed ? 'Closed' : 'Open'}</td>
        </tr>
    );
}

export default EventRow;
