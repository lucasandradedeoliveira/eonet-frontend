import React, { useState, useEffect } from 'react';
import EventRepository from './Repositories/EventRepository';
import EventsTable from './Components/EventsTable';
import EventModal from './Components/EventModal';
import Form from './Components/Form';
import NavBar from './Components/NavBar';

function App() {
    const [loading, setLoading] = useState(true);
    const [events, setEvents] = useState([]);
    const [status, setStatus] = useState('open');
    const [days, setDays] = useState(null);
    const [order, setOrder] = useState(null);
    const [event, setEvent] = useState(null);

    useEffect(() => {
        async function loadEvents() {
            try {
                const response = await EventRepository.list(status, days, order);
                setEvents(response);
            } catch (e) {
                // handle error
            } finally {
                setLoading(false);
            }
        }

        loadEvents();
    }, [status, days, order]);

    const loadEvent = async eventId => {
        try {
            const response = await EventRepository.get(eventId);
            setEvent(response);
        } catch (e) {
            setEvent(null);
        }
    };

    const onChangeOrder = o => {
        if (loading) {
            return;
        }

        setLoading(true);
        setOrder(o === order ? null : o);
    };

    const onChangeDays = d => {
        if (loading) {
            return;
        }

        setLoading(true);
        setDays(d);
    };

    const onToggleStatus = () => {
        if (loading) {
            return;
        }

        setLoading(true);
        setStatus(status !== 'open' ? 'open' : 'closed');
    };

    const onClickEvent = e => {
        if (loading) {
            return;
        }

        setEvent({ id: e });
        loadEvent(e);
    };

    return (
        <div>
            <NavBar />
            <div className="container mt-5 mb-5">
                <div className="table-wrapper">
                    <Form
                        status={status}
                        order={order}
                        onChangeOrder={onChangeOrder}
                        onChangeDays={onChangeDays}
                        onToggleStatus={onToggleStatus} />
                    <EventsTable events={events} loading={loading} onClickEvent={onClickEvent} />
                </div>
            </div>
            <EventModal event={event} onClose={() => setEvent(null)} />
        </div>
    );
}

export default App;
